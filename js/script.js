var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

canvas.width = canvas.clientWidth;
canvas.height = canvas.clientHeight;

var x = canvas.width/2;
var y = canvas.height - 50;
var ballRadius = 12;
var dx = 4;
var dy = -4;
var speed = 4;
var paddleHeight = 12;
var paddleWidth = 125;
var paddleX = (canvas.width - paddleWidth)/2;
var paddleY = canvas.height - 30;
var paddleDx = 0;
var brickRowCount = 1;
var brickColumntCount = 6;
var brickWidth = 93;
var brickHeight = 25;
var brickPaddingX = 10;
var brickPaddingY = 15;
var brickOffsetTop = 40;
var brickOffsetLeft = 15;
var score = 0;
var totalscore = 0;
var lives = 2;
var lvl = 1;
var maxlvl = 2;
var pause = false;
var game_over = false;
var hit = false;
const LIFE_IMG = new Image();
LIFE_IMG.src = "img/heart.png";

const LVL_IMG = new Image();
LVL_IMG.src = "img/level.png";

const restart = document.getElementById("restart");

restart.addEventListener("click", function(){
    location.reload();
})

const gameover = document.getElementById("gameover");
const youlose = document.getElementById("youlose");
const next_lvl = document.getElementById("next_lvl");
const youwon = document.getElementById("youwon");

function youLose(){
    gameover.style.display = "block";
    youlose.style.display = "block";
	restart.style.display = "block";
	
	setTimeout(function(){
								alert("Your score : " + totalscore);
							}, 200);
	
}

function youWon(){
    gameover.style.display = "block";
    youwon.style.display = "block";
	restart.style.display = "block";
	
	setTimeout(function(){
								alert("Your score : " + totalscore);
								save();
							}, 200);
	
}

function nextLvl(){
    gameover.style.display = "block";	
	next_lvl.style.display = "block";
	lvl++;
	brickRowCount++;
	initBricks(lvl);
	score = 0;
	dx += 1;
	dy = -dy;
	speed++;
	dy -= 1;
	x = canvas.width/2;
	y = canvas.height - 50;
	paddleX = (canvas.width-paddleWidth)/2;
	hit = false;
}


var bricks = [];
initBricks(1);
function initBricks(value){
	for (c=0; c<brickColumntCount; c++){
		bricks[c] = [];
		for (r=0; r<brickRowCount; r++){
			bricks[c][r] = {x: 0, y: 0, status: value}
		}
	}
}

function drawBricks() {
	for(c=0; c<brickColumntCount; c++) {
		for(r=0; r<brickRowCount; r++) {
			if(bricks[c][r].status !=0){
				var brickX = (c*(brickWidth + brickPaddingX))+ brickOffsetLeft;
				var brickY = (r*(brickHeight + brickPaddingY))+ brickOffsetTop;
				bricks[c][r].x = brickX;
				bricks[c][r].y = brickY;
				if(bricks[c][r].status ==1){
					ctx.beginPath();
					ctx.rect(brickX, brickY, brickWidth, brickHeight);
					ctx.fillStyle = "gray";
					ctx.fill();
					ctx.strokeStyle = "dark";
					ctx.strokeRect(brickX, brickY, brickWidth, brickHeight);
					ctx.closePath();
				}else if(bricks[c][r].status ==2){
					ctx.beginPath();
					ctx.rect(brickX, brickY, brickWidth, brickHeight);
					ctx.fillStyle = "DarkGreen";
					ctx.fill();
					ctx.strokeStyle = "dark";
					ctx.strokeRect(brickX, brickY, brickWidth, brickHeight);
					ctx.closePath();
				}
			}
		}
	}
}

function drawBall(color) {
	ctx.beginPath();
	ctx.arc(x, y, ballRadius, 0, Math.PI*2);
	ctx.fillStyle = color;
    ctx.fill();
    
    ctx.strokeStyle = "dark";
    ctx.stroke();
	ctx.closePath();
}

function drawPaddle() {
	ctx.beginPath();
	ctx.fillStyle = "DimGray";
    ctx.fillRect(paddleX, paddleY, paddleWidth, paddleHeight);
    
    ctx.strokeStyle = "dark";
    ctx.strokeRect(paddleX, paddleY, paddleWidth, paddleHeight);
	ctx.closePath();
}

function drawScore(){
	ctx.font = "25px Arial";
	ctx.fillStyle = "red";
	ctx.fillText("SCORE: "+totalscore, 15, 30);
}

function drawLives(){
	ctx.font = "26px Arial";
	ctx.fillStyle = "red";
	ctx.fillText(lives, canvas.width-30, 30);
	ctx.drawImage(LIFE_IMG, canvas.width-70,4);
}

function drawLevel(){
	ctx.font = "26px Arial";
	ctx.fillStyle = "red";
	ctx.fillText(lvl, canvas.width/2 + 40, 30);
	ctx.drawImage(LVL_IMG, canvas.width/2,4);
}

function collisionDetectionBricks(){
	for(c=0; c<brickColumntCount; c++){
		for(r=0; r<brickRowCount; r++){
			var b = bricks[c][r];
			if(b.status != 0){
				if(x + ballRadius > b.x && x - ballRadius < b.x + brickWidth && y + ballRadius > b.y && y - ballRadius < b.y + brickHeight){
					if(!hit){
					dy = -dy;
					hit=true;}
					b.status--;
					if(b.status ==0){
						score +=100;
						totalscore +=100;
					}
					if(score == brickColumntCount * brickRowCount *100){
						if(lvl == maxlvl)
						{
							youWon();
							game_over = true;
						}else{
							nextLvl();
							pause = true;
							setTimeout(function(){
								pause = false;
								draw();
								gameover.style.display = "none";	
								next_lvl.style.display = "none";
							}, 2000);
						}
					}
				}
			}	
		}
	}
}

function collisionDetection(){
	if(y + dy < ballRadius) {
		dy = -dy;
	}
	else if(y + ballRadius > paddleY ) {
		if(x > paddleX && x < paddleX + paddleWidth) {
			var collidePoint = x - (paddleX + paddleWidth/2);
			collidePoint = collidePoint / (paddleWidth/2);
			var angle = collidePoint * Math.PI/3;
			dx = speed * Math.sin(angle);
			dy = - speed * Math.cos(angle);
			hit = false;
		}
		else if(y + ballRadius > canvas.height && !game_over) {
			lives--;
			
			if(lives == 0){
				pause = true;
				drawBall("red");
				draw();
				setTimeout(function(){
								pause = false;
								youLose();
							}, 200);
				game_over = true;
			} else{
				pause = true;
				drawBall("red");
				setTimeout(function(){
								if(game_over == false){
									pause = false;
									x = canvas.width/2;
									y = canvas.height -50;
									dy = -dy;
									dx = -dx;
									paddleX = (canvas.width-paddleWidth)/2;
									hit = false;
									draw();
								}
							}, 2000);
			}
		}
	}
		
	if(x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
		dx = -dx;
	}
}

window.addEventListener("deviceorientation", handleOrientation, true);
function handleOrientation(event) {
    var absolute = event.absolute;
    var alpha = event.alpha;
    var beta = event.beta;
    var gamma = event.gamma;
	
	//console.log(gamma);
	if(beta > 1 && beta <= 3)
	{
		paddleDx = 2;
	}
	else if(beta > 3 && beta <= 7)
	{
		paddleDx = 2.5;
	}
	else if(beta > 7 && beta <= 12)
	{
		paddleDx = 3;
	}
	else if(beta > 12 && beta <= 18)
	{
		paddleDx = 4;
	}
	else if(beta > 18 && beta <= 25)
	{
		paddleDx = 5;
	}
	else if(beta > 25 )
	{
		paddleDx = 6.5;
	}
	else if(beta < -1 && beta >= -3)
	{
		paddleDx = -2;
	}
	else if(beta < -3 && beta >= -7)
	{
		paddleDx = -2.5;
	}
	else if(beta < -7 && beta >= -12)
	{
		paddleDx = -3;
	}
	else if(beta < -12 && beta >= -18)
	{
		paddleDx = -4;
	}
	else if(beta < -18 && beta >= -25)
	{
		paddleDx = -5;
	}
	else if(beta < -18 )
	{
		paddleDx = -6.5;
	}
	else
	{
		paddleDx = 0;
	}
}

function movePaddle(){
	if(paddleX < canvas.width - paddleWidth && paddleX > 4){
		paddleX += paddleDx;
	}
	else if(paddleX >= canvas.width - paddleWidth){
		if(paddleDx < 0)
		{
			paddleX +=paddleDx;
		}
	}else if( paddleX <= 4)
	{
		if(paddleDx > 0)
		{
			paddleX +=paddleDx;
		}
	}
	
}

function draw() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	drawBall("DodgerBlue");
	drawPaddle();
	drawBricks();
	drawScore();
	drawLives();
	drawLevel();
	movePaddle();
	collisionDetection();
	collisionDetectionBricks();
	
	if(!game_over && !pause){
		requestAnimationFrame(draw);
	}
	
	x += dx;
	y += dy;
}

draw();
